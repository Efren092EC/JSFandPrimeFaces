/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.MenuFacadeLocal;
import com.empresa.agenda.model.Menu;
import com.empresa.agenda.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;

import javax.inject.Named;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author efren
 */
@Named
@SessionScoped
public class MenuController implements Serializable {

    @EJB
    private MenuFacadeLocal EJBmanu;
    private List<Menu> lista;
    private MenuModel model;

    @PostConstruct
    public void init() {
        this.listarMenus();
        model = new DefaultMenuModel();
        this.establecerPermisos();
    }

    public void listarMenus() {
        try {
            lista = EJBmanu.findAll();
        } catch (Exception e) {
        }
    }

    public MenuModel getModel() {
        return model;
    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public void establecerPermisos() {
        FacesContext contex = FacesContext.getCurrentInstance();
        Usuario us = (Usuario) contex.getExternalContext().getSessionMap().get("usuario");
        for (Menu m : lista) {
            if (m.getTipo().equals("S") && m.getTipoUsuario().endsWith(us.getTipo())) {
                DefaultSubMenu firstSubmenu = new DefaultSubMenu(m.getNombre());
                for (Menu i : lista) {
                    Menu submenu = i.getCodigoSubmeno();
                    if (submenu != null) {
                        if (submenu.getCodigo() == m.getCodigo()) {
                            DefaultMenuItem item = new DefaultMenuItem(i.getNombre());
                            item.setUrl(i.getUrl());
                            firstSubmenu.addElement(item);
                        }

                    }
                }
                model.addElement(firstSubmenu);
            } else {
                if (m.getCodigoSubmeno() == null && m.getTipoUsuario().endsWith(us.getTipo())) {
                    DefaultMenuItem item = new DefaultMenuItem(m.getNombre());
                     item.setUrl(m.getUrl());
                    model.addElement(item);
                }

            }
        }
    }
    public void cerrarSesion(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }
    
    public  String mostrarNombreUsuarioLogeado(){
    
        Usuario us = (Usuario)  FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        
        return us.getUsuario();
    }
}
