/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.PersonaFacadeLocal;
import com.empresa.agenda.ejb.TelefonoFacadeLocal;
import com.empresa.agenda.model.Persona;
import com.empresa.agenda.model.Telefono;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author efren
 */
@Named
@ViewScoped
public class ConsultarTelefonoController implements Serializable {

    @EJB
    private PersonaFacadeLocal personaEJB;

    @EJB
    private TelefonoFacadeLocal telefonoEJB;

    private List<Persona> listapersonas;
    private List<Telefono> telefonos;
    private int codigoPersona;

    public List<Persona> getListapersonas() {
        return listapersonas;
    }

    public void setListapersonas(List<Persona> listapersonas) {
        this.listapersonas = listapersonas;
    }

    public int getCodigoPersona() {
        return codigoPersona;
    }

    public void setCodigoPersona(int codigoPersona) {
        this.codigoPersona = codigoPersona;
    }

    @PostConstruct
    public void init() {
        listapersonas = personaEJB.findAll();
    }

    public List<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = telefonos;
    }

    public void buscarTelefonos() {
        try {
            telefonos = telefonoEJB.buscarTelefoPersona(codigoPersona);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
