/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.model.Usuario;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author efren
 */

@Named
@ViewScoped
public class PlantillaController  implements Serializable{
    public void verificarSesion(){
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
           Usuario us = (Usuario) contex.getExternalContext().getSessionMap().get("usuario");
            if (us == null) {
                contex.getExternalContext().redirect("../permisos.xhtml");
            } else {
            }
        } catch (Exception e) {
            
        }
    }
}
