/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.CategoriaFacadeLocal;
import com.empresa.agenda.ejb.NotaFacadeLocal;
import com.empresa.agenda.model.Categoria;
import com.empresa.agenda.model.Nota;
import com.empresa.agenda.model.Persona;
import com.empresa.agenda.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author efren
 */
@Named
@ViewScoped
public class NotaController implements Serializable {

    @EJB
    private NotaFacadeLocal notaEJB;

    @EJB
    private CategoriaFacadeLocal categoriaEJB;
    @Inject
    private Nota nota;

    @Inject
    private Categoria categoria;

    private List<Categoria> categorias;

    @PostConstruct
    public void init() {
        categorias = categoriaEJB.findAll();
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public Nota getNota() {
        return nota;
    }

    public void setNota(Nota nota) {
        this.nota = nota;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void resgistrar() {
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
            Usuario us = (Usuario) contex.getExternalContext().getSessionMap().get("usuario");
            nota.setCategoria(categoria);
            nota.setPersona(us.getCodigo());
            notaEJB.create(nota);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Registro de Nota Exitoso"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "La Nota No Ha Sido Guardada"));
        }

    }
}
