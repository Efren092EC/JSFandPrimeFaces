/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.CategoriaFacadeLocal;
import com.empresa.agenda.ejb.NotaFacadeLocal;
import com.empresa.agenda.model.Categoria;
import com.empresa.agenda.model.Nota;
import com.empresa.agenda.model.Usuario;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author efren
 */
@Named
@ViewScoped
public class BuscarController implements Serializable {

    @EJB
    private CategoriaFacadeLocal categoriaEJB;

    @EJB
    private NotaFacadeLocal notaEJB;

    private List<Categoria> categorias;
    private List<Nota> notas;
    private int codigoCategria;
    private Date fechaConsulta;

    @PostConstruct
    public void init() {
        categorias = categoriaEJB.findAll();
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public int getCodigoCategria() {
        return codigoCategria;
    }

    public void setCodigoCategria(int codigoCategria) {
        this.codigoCategria = codigoCategria;
    }

    public Date getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(Date fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }

    public void buscar() {
        try {
            Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            notas = notaEJB.buscar(us.getCodigo().getCodigo(), codigoCategria, fechaConsulta);
        } catch (Exception e) {
        }
    }
}
