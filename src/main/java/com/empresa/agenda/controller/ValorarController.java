/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.NotaFacadeLocal;
import com.empresa.agenda.model.Nota;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author efren
 */
@Named
@ViewScoped
public class ValorarController implements Serializable {
    
    @EJB
    private NotaFacadeLocal notaEJB;
    @Inject
    private ComentarController comentarcontroller;
    
    private Nota nota;
    
    @PostConstruct
    public void init() {
        this.nota = comentarcontroller.getNota();
    }
    
    public Nota getNota() {
        return nota;
    }
    
    public void setNota(Nota nota) {
        this.nota = nota;
    }

    public void registrar() {
        try {
            notaEJB.edit(nota);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Valoracion y Comentario Registrados de forma Exitosa"));
        } catch (Exception e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se pudeo Registrar la Valoracion y Comentario"));
        }finally{
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
        
    }
}
