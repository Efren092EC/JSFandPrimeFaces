/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.agenda.controller;

import com.empresa.agenda.ejb.TelefonoFacadeLocal;
import com.empresa.agenda.model.Telefono;
import com.empresa.agenda.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author efren
 */
@Named
@ViewScoped
public class TelefonoController implements Serializable {

    @EJB
    private TelefonoFacadeLocal telefonoEJB;

    @Inject
    private Telefono telefono;

    private List<Telefono> listatelefonos;
    private String accion;

    public Telefono getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefono telefono) {
        this.telefono = telefono;
    }

    public List<Telefono> getListatelefonos() {
        return listatelefonos;
    }

    public void setListatelefonos(List<Telefono> listatelefonos) {
        this.listatelefonos = listatelefonos;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    @PostConstruct
    public void init() {
        listatelefonos = telefonoEJB.findAll();
    }

    public void registar() {
        Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
        telefono.setPersona(us.getCodigo());
        telefonoEJB.create(telefono);
        listatelefonos = telefonoEJB.findAll();
    }

    public void leer(Telefono telselecion) {
        telefono = telselecion;
        this.setAccion("M");
    }

    public void modificar(Telefono tel) {
        try {
             telefonoEJB.edit(tel);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Cambio  Registro con Exitoso"));
        } catch (Exception e) {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Cambio No Registrado"));
        }
       
    }
}
